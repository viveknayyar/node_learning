var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var root = __dirname + '/app'
var multer = require('multer');
var path = require('path');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('aws_config.json');
var fs = require('fs');
var s3 = new AWS.S3();
var port = process.env.PORT || 7000;
var router = express.Router();

var storage = multer.diskStorage({
	destination: function (req, file, callback) {
		callback(null, './uploads')
	},
	filename: function (req, file, callback) {
		callback(null, file.originalname.split('.')[0] + '-' + Date.now() + path.extname(file.originalname));
	}
});

var upload = multer({ storage: storage });

router.use(function(req, res, next) {
	console.log('Inside Middleware.');
	next();
});

router.get('/', function(req, res) {
	res.sendFile(root + "/index.html");
});

router.post('/upload/photo', upload.array('displayImage'), function(req, res, next) {
	console.log(req.files);
	uploadFiles(req.files,function(response) {
		res.json(response)
	});
});

app.use('/local.craftsvilla.com', router);
app.use(express.static(path.join(__dirname, 'app')));

app.listen(port, function() {
	console.log("Magic happens on port " + port);
});

function uploadFiles(files, callback) {
	files.forEach(function(file) {
		uploadFile(file.filename,'./uploads/' + file.filename , callback);
	});
}

function uploadFile(remoteFilename, fileName, callback) {
	var fileBuffer = fs.readFileSync(fileName);
	var metaData = getContentTypeByFile(fileName);
	s3.upload({
		ACL: 'public-read',
		Bucket: 'petzoned',
		Key: remoteFilename,
		Body: fileBuffer,
		ContentType: metaData
	}, function(error, response) {
		console.log('uploaded file[' + fileName + '] to [' + remoteFilename + '] as [' + metaData + ']');
		console.log(response);
		callback(response);
	});
}

function getContentTypeByFile(fileName) {
  var rc = 'application/octet-stream';
  var fn = fileName.toLowerCase();

  if (fn.indexOf('.png') >= 0) rc = 'image/png';
  else if (fn.indexOf('.jpg') >= 0) rc = 'image/jpg';
  else if (fn.indexOf('.jpeg') >= 0) rc = 'image/jpeg';

  return rc;
}

