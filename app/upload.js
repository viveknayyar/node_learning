$(document).ready(function() {
	Dropzone.autoDiscover = false;
	Dropzone.options.myDropzone = false;
	if ($('div#fileUpload').length) {
		var myDropzone = new Dropzone("div#fileUpload", {
			url: "/local.craftsvilla.com/upload/photo",
			autoProcessQueue:true,
			addRemoveLinks:true,
			parallelUploads: 10,
			paramName: 'displayImage'
		});
		myDropzone.on("addedfile", function(file,responseText) {
			console.log(file);
		});
		myDropzone.on("success", function(file,responseText) {
			console.log(responseText);
			var html = '<div class="row margin-0"> \
							<div class="col-md-6"> \
								<div class="cell"> \
									<div class="filename upload_name">' + responseText.key + ' \
									</div> \
								</div> \
							</div> \
							<div class="col-md-6"> \
								<div class="cell"> \
									<div class="location upload_name">' + responseText.Location + ' \
									</div> \
								</div> \
							</div> \
						</div>' ;
			$('.row_container').append(html);
		});
		myDropzone.on("queuecomplete", function (file) {
			alert("All files have uploaded ");
		});
	}
});
